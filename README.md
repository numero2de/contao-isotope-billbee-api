# ⚠ Commercial module, do not publish!


Isotope Config
- Shop config save shop config to generate an "API Key" or set one yourself under the billbee legend
- optional configure oderstatus and payment to set the values used at billbee under the billbee legend

Configure at Billbee
- goto "Einstellungen" -> "Verbindungen" -> "Shops". Click "Neu" and select "Eigener Webshop (Billbee API)"
- Enter the need information and the following fields as described
  - "URL": domain to your shop + /billbee_api/ + id of the shop config. For example https://example.com/billbee_api/1
  - "Schlüssel": API Key enter or generate at the shop config in Contao/Isotope
  - "Änderungen abgleichen": set to "customshop-> Billbee"
- you can test the connection information to the shop by pressing "Verbindung testen"
