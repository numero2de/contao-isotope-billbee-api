<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2019 Leo Feyer
 *
 * @package   Isotope Billbee Api
 * @author    Benny Born <benny.born@numero2.de>
 * @author    Michael Bösherz <michael.boesherz@numero2.de>
 * @license   LGPL
 * @copyright 2019 numero2 - Agentur für digitales Marketing
 */


namespace numero2\IsotopeBillbeeApi\Controller;

use numero2\IsotopeBillbeeApi\Action\GetOrders;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Isotope\Model\Config;

/**
 * Handles the Marketing Suite Client API requests.
 *
 * @Route("/billbee_api", defaults={"_scope" = "frontend", "_token_check" = false})
 */
class IsotopeBillbeeApiController extends Controller {


    /**
     * @Route("/{shopconfig}", requirements={"shopconfig"="\d+"}, methods={"GET"})
     */
    public function actions($shopconfig, Request $request) {

        $this->initializeContao();

        $oConfig = Config::findOneById($shopconfig);

        if( !$oConfig ) {
            $response = new JsonResponse(['status'=>"error", "message"=>"Given id not found"]);
            $response->setStatusCode('404');

            return $this->send($request, $response);
        }

        if( !$this->checkSignature($oConfig, $request->query->get('Key')) ) {

            $response = new JsonResponse(['status'=>"error", "message"=>"Could not verify signature"]);
            $response->setStatusCode('401');

            return $this->send($request, $response);
        }

        $action = $request->query->get('Action');

        $clsAction = null;
        if( $action === "GetOrders" ) {
            $clsAction = new GetOrders();
        }

        if( $clsAction ) {
            $response = $clsAction->getResponse($oConfig, $request);
        } else {

            $response = new JsonResponse(['status'=>"error", "message"=>"Action not found"]);
            $response->setStatusCode('404');
        }


        return $this->send($request, $response);
    }


    /**
     * Return and log response
     *
     * @param  Request  $request
     * @param  Response $response
     * @return Response
     */
    private function send(Request $request, Response $response) {

        $strData = "";

        if( $request->getMethod() == "GET" ) {
            $strData = $request->getQueryString();
        } else {
            $strData = $request->getContent();
        }

        // log request
        $strLogsDir = null;
        $strLogsDir = $this->container->getParameter('kernel.logs_dir');

        error_log(
            sprintf(
                "[%s] [%s] [%s] [%s] %s\n"
            ,   date('d-m-Y H:i:s')
            ,   $request->getMethod()
            ,   $request->getPathInfo()
            ,   $response->getStatusCode()
            ,   $strData
            )
        ,   3
        ,   $strLogsDir . '/billbee-api-'.date('Y-m-d').'.log'
        );

        return $response;
    }


    /**
     * Verify the signature with the api key set in the billbee settings
     *
     * @param \Isotope\Model\Config $oConfig
     * @param string $sign
     *
     * @return boolean
     */
    private function checkSignature($oConfig, $sign) {

        $timestamp = substr(time(), 0, 7);
        $apiKey = $oConfig->billbee_api_key;

        if( !strlen($apiKey) ) {
            return true;
        }

        $hash = hash_hmac( "sha256", utf8_encode($apiKey), utf8_encode($timestamp));

        // encode with base64
        $bsec = base64_encode($hash);

        // remove certain charecters
        $bsec = str_replace("=","",$bsec);
        $bsec = str_replace("/","",$bsec);
        $bsec = str_replace("+","",$bsec);

        return $bsec === $sign;
    }

    /**
     * Initializes the Contao framework
     */
    private function initializeContao() {

        if( $this->container->has('contao.framework') && !$this->container->get('contao.framework')->isInitialized() ) {
            $this->container->get('contao.framework')->initialize();
        }
    }
}
