<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2019 Leo Feyer
 *
 * @package   Isotope Billbee Api
 * @author    Benny Born <benny.born@numero2.de>
 * @author    Michael Bösherz <michael.boesherz@numero2.de>
 * @license   LGPL
 * @copyright 2019 numero2 - Agentur für digitales Marketing
 */


namespace numero2\IsotopeBillbeeApi;

use Symfony\Component\HttpKernel\Bundle\Bundle;


/**
 * Configures the Isotope Billbee Api Bundle.
 */
class IsotopeBillbeeApiBundle extends Bundle
{
}
