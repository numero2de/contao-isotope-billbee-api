<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2019 Leo Feyer
 *
 * @package   Isotope Billbee Api
 * @author    Benny Born <benny.born@numero2.de>
 * @author    Michael Bösherz <michael.boesherz@numero2.de>
 * @license   LGPL
 * @copyright 2019 numero2 - Agentur für digitales Marketing
 */


namespace numero2\IsotopeBillbeeApi\Action;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Contao\Database;
use Contao\System;
use Isotope\Model\Address;
use Isotope\Model\ProductCollectionItem;
use Isotope\Model\ProductCollectionSurcharge;
use Isotope\Model\TaxRate;


class GetOrders {

    /* time format used at billbee */
    private $timeFormat = \DateTimeInterface::ISO8601;


    /**
     * constructor initialises the address cache
     */
    public function __construct() {
        $this->aAddressCache = [];
    }


    /**
     * peforms the action and returns the response
     *
     * @param string $oConfig
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getResponse($oConfig, Request $request) {

        $start = $request->query->get('StartDate');
        if( $start && preg_match("/\d{4}-\d{2}-\d{2}/", $start) ) {
            $start = strtotime($start);
        } else {
            $start = null;
        }
        $page = intval($request->query->get('Page'));
        $pageSize = intval($request->query->get('PageSize'));

        $response = null;
        if( empty($start) || !is_int($start) || $start < 0 ) {
            $response = new JsonResponse(['status'=>'error', 'message'=>'Missing "StartDate" or wrong format.']);
        }

        if( empty($page) || !is_int($page) || $page < 0 ) {
            $response = new JsonResponse(['status'=>'error', 'message'=>'Missing "Page" or wrong format.']);
        }

        if( empty($pageSize) || !is_int($pageSize) || $pageSize < 0 ) {
            $response = new JsonResponse(['status'=>'error', 'message'=>'Missing "PageSize" or wrong format.']);
        }

        // return response with error code and message
        if( $response ) {
            $response->setStatusCode('400');

            return $response;
        }

        $aResult = [];
        $aResult['paging']['page'] = $page;
        $itemCount = $this->countChangeOrders($oConfig->id, $start);
        $aResult['paging']['totalCount'] = $itemCount;
        $aResult['paging']['totalPages'] = ceil($itemCount/$pageSize);

        if( $itemCount > 0 ) {

            $aResult['orders'] = array_values($this->getChangeOrders($oConfig->id, $start, $page, $pageSize));

            array_walk_recursive($aResult, function (&$item,$key) {
                if( is_string($item) ) {
                    $item = html_entity_decode($item);
                }
            });
        }

        $response = new JsonResponse($aResult);
        $response->setStatusCode('200');

        return $response;
    }


    /**
     * Counts the total number of new or changed orders
     *
     * @param string $shopConfigId
     * @param integer $start
     *
     * @return integer
     */
    public function countChangeOrders($shopConfigId, $start) {

        $objResult = Database::getInstance()->prepare("
            SELECT count(1) AS count
            FROM tl_iso_product_collection
            WHERE type=? AND config_id=? AND (locked>=? OR tstamp>=?)
        ")->execute('order', $shopConfigId, $start, $start);

        return $objResult->count;
    }


    /**
     * Gets all changed or new orders since start
     *
     * @param string $shopConfigId
     * @param integer $start
     * @param integer $page
     * @param integer $pageSize
     *
     * @return array;
     */
    public function getChangeOrders($shopConfigId, $start, $page, $pageSize) {

        $objResult = Database::getInstance()->prepare("
            SELECT
                c.id,
                c.locked,
                c.tstamp,
                c.member,
                c.document_number,
                c.date_paid,
                c.date_shipped,
                c.shipping_id,
                c.payment_id,
                c.billing_address_id,
                c.shipping_address_id,
                c.subtotal,
                c.tax_free_subtotal,
                c.total,
                c.tax_free_total,
                c.currency,
                c.language,
                c.notes,
                c.coupons,
                o.billbee_status,
                p.billbee_payment
            FROM tl_iso_product_collection AS c
            JOIN tl_iso_orderstatus AS o ON (c.order_status = o.id)
            JOIN tl_iso_payment AS p ON (c.payment_id = p.id)
            WHERE c.type=? AND c.config_id=? AND (c.locked>=? OR c.tstamp>=?)
            ORDER BY c.locked ASC
        ")->limit($pageSize, ($page-1)*$pageSize)->execute('order', $shopConfigId, $start, $start);

        $aReturn = [];

        if( $objResult && $objResult->numRows ) {

            $aResults = $objResult->fetchAllAssoc();

            foreach( $aResults as $aRow ) {

                $id = $aRow['id'];

                $aReturn[$id]['order_id'] = $aRow['id'];
                $aReturn[$id]['order_number'] = $aRow['document_number'];
                $aReturn[$id]['order_status_id'] = $aRow['billbee_status'];
                $aReturn[$id]['order_date'] = date($this->timeFormat, $aRow['locked']);
                // $aReturn[$id]['UpdatedAt'] = date($this->timeFormat, $aRow['tstamp']);
                if( $aRow['date_paid'] > 0 ) {
                    $aReturn[$id]['pay_date'] = date($this->timeFormat, $aRow['date_paid']);
                }
                if( $aRow['date_shipped'] > 0 ) {
                    $aReturn[$id]['ship_date'] = date($this->timeFormat, $aRow['date_shipped']);
                }
                if( strlen($aRow['notes']) ) {
                    $aReturn[$id]['seller_comment'] = $aRow['notes'];
                }
                $aReturn[$id]['invoice_number'] = $aRow['document_number'];
                $aReturn[$id]['invoice_date'] = date($this->timeFormat, $aRow['locked']);
                $aReturn[$id]['invoice_address'] = $this->getAddress($aRow['billing_address_id']);
                if( array_key_exists('email', $aReturn[$id]['invoice_address']) ) {
                    $aReturn[$id]['email'] = $aReturn[$id]['invoice_address']['email'];
                    unset($aReturn[$id]['invoice_address']['email']);
                }
                $aReturn[$id]['delivery_address'] = $this->getAddress($aRow['shipping_address_id']);
                if( array_key_exists('email', $aReturn[$id]['delivery_address']) ) {
                    unset($aReturn[$id]['delivery_address']['email']);
                }
                $aReturn[$id]['payment_method'] = $aRow['billbee_payment'];

                // $aReturn[$id]['TotalCost'] = (float)$aRow['tax_free_subtotal'];
                $aReturn[$id]['currency_code'] = $aRow['currency']; // check

                $aReturn[$id]['order_products'] = $this->getItems($aRow['id']);
                $aReturn[$id]['ship_cost'] = $this->getShipping($aRow['id']);

                // "AdjustmentCost": 0,    // (number, optional)
                // "AdjustmentReason": "string",    // (string, optional)
            }
        }

        return $aReturn;
    }


    /**
     * Gets the adress by the given ID
     *
     * @param string $addressId
     *
     * @return array
     */
    private function getAddress($addressId) {

        $aCache = $this->aAddressCache;

        //check if address is in chache
        if( array_key_exists($addressId, $aCache) ) {
            return $aCache[$addressId];
        }

        $oAddress = Address::findOneById($addressId);

        $aAddress = [];

        if( strlen($oAddress->company) ) {
            $aAddress['company'] = $oAddress->company;
        }
        if( strlen($oAddress->street_1) ) {

            if( strpos($oAddress->street_1, ' ') !== false ) {
                $aParts = explode(' ', $oAddress->street_1);
                $last = array_pop($aParts);
                $aAddress['street'] = implode(' ', $aParts);
                $aAddress['housenumber'] = $last;
            } else {
                $aAddress['street'] = $oAddress->street_1;
            }

        }
        if( strlen($oAddress->street_2) ) {
            $aAddress['address2'] = $oAddress->street_2;
        }
        if( strlen($oAddress->street_3) ) {
            $aAddress['address3'] = $oAddress->street_3;
        }
        if( strlen($oAddress->city) ) {
            $aAddress['city'] = $oAddress->city;
        }
        if( strlen($oAddress->postal) ) {
            $aAddress['postcode'] = $oAddress->postal;
        }
        if( strlen($oAddress->subdivision) ) {
            $aAddress['state'] = $oAddress->subdivision;
        }
        if( strlen($oAddress->country) ) {
            $aAddress['country_code'] = $oAddress->country;
        }
        if( strlen($oAddress->firstname) ) {
            $aAddress['firstname'] = $oAddress->firstname;
        }
        if( strlen($oAddress->lastname) ) {
            $aAddress['lastname'] = $oAddress->lastname;
        }
        if( strlen($oAddress->email) ) {
            $aAddress['email'] = $oAddress->email;
        }

        $aCache[$addressId] = $aAddress;
        $this->aAddressCache = $aCache;

        return $aAddress;
    }


    /**
     * Get all Items for the given order
     *
     * @param string $collectionId
     *
     * @return array
     */
    private function getItems($collectionId) {

        $oItems = ProductCollectionItem::findBy(['pid=?'], [$collectionId]);

        $oTaxRates = TaxRate::findAll();
        $aTaxRates = [];

        if( $oTaxRates ) {
            foreach( $oTaxRates as $oTaxRate ) {

                $aTaxRates[$oTaxRate->id] = deserialize($oTaxRate->rate);
            }
        }

        $aItems = [];
        if( $oItems ) {
            foreach( $oItems as $oItem ) {
                $aRow = [];

                $aRow['product_id'] = $oItem->product_id;
                $aRow['name'] = $oItem->name;
                $aRow['sku'] = $oItem->sku;
                $aRow['quantity'] = (float)$oItem->quantity;
                $aRow['unit_price'] = (float)$oItem->price;
                $aRow['discount_percent'] = 0.00;

                if( $oItem->tax_id ) {
                    $aTaxes = [];

                    if( strpos($oItem->tax_id, ',') !== false ) {
                        $aTaxes = explode(',', $oItem->tax_id);
                    }
                    $aTaxes = (array)$oItem->tax_id;

                    $aRow['tax_rate'] = 0.00;
                    foreach( $aTaxes as $tax ) {

                        if( array_key_exists($tax, $aTaxRates) ) {
                            if( $aTaxRates[$tax]['unit'] == '%' ) {
                                $aRow['tax_rate'] += floatval($aTaxRates[$tax]['value']);
                            }
                        }
                    }
                }

                $options = $oItem->getConfiguration();
                if( !empty($options) ) {
                    foreach( $options as $option ) {

                        $aRow['options'][] = [
                            "name" => $option['label']
                        ,   "value" => (string) $option
                        ];
                    }
                }

                $aItems[] = $aRow;
            }
        }

        return $aItems;
    }


    /**
     * get the sHipping costs for the given order
     *
     * @param string $collectionId
     *
     * @return integer
     */
    private function getShipping($collectionId) {

        $oShipping = ProductCollectionSurcharge::findOneBy(['pid=? AND type=?'], [$collectionId, 'shipping']);

        if( $oShipping ) {

            return (float)$oShipping->tax_free_total_price;
        }

        return 0;
    }
}
