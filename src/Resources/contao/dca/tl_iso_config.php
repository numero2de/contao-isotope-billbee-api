<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2019 Leo Feyer
 *
 * @package   Isotope Billbee Api
 * @author    Benny Born <benny.born@numero2.de>
 * @author    Michael Bösherz <michael.boesherz@numero2.de>
 * @license   LGPL
 * @copyright 2019 numero2 - Agentur für digitales Marketing
 */


/**
 * Add palettes to tl_iso_config
 */
$GLOBALS['TL_DCA']['tl_iso_config']['palettes']['default'] = str_replace(
    '{analytics_legend}'
,   '{billbee_legend:hide},billbee_api_key;{analytics_legend}'
,   $GLOBALS['TL_DCA']['tl_iso_config']['palettes']['default']
);


/**
 * Add fields to tl_iso_config
 */
$GLOBALS['TL_DCA']['tl_iso_config']['fields']['billbee_api_key'] = [
    'label'             => &$GLOBALS['TL_LANG']['tl_iso_config']['billbee_api_key']
,   'exclude'           => true
,   'inputType'         => 'text'
,   'save_callback'     => [['numero2\IsotopeBillbeeApi\DCAHelper\IsotopeConfig','generateApiKey']]
,   'eval'              => ['max-length'=>64, 'tl_class'=>'w50']
,   'sql'               => "varchar(64) NOT NULL default ''"
];
