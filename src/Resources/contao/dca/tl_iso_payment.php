<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2019 Leo Feyer
 *
 * @package   Isotope Billbee Api
 * @author    Benny Born <benny.born@numero2.de>
 * @author    Michael Bösherz <michael.boesherz@numero2.de>
 * @license   LGPL
 * @copyright 2019 numero2 - Agentur für digitales Marketing
 */


/**
 * Modify config of tl_iso_payment
 */
$GLOBALS['TL_DCA']['tl_iso_payment']['config']['onload_callback'][] = ['numero2\IsotopeBillbeeApi\DCAHelper\IsotopePayment', 'onload'];


/**
 * Add fields to tl_iso_payment
 */
$GLOBALS['TL_DCA']['tl_iso_payment']['fields']['billbee_payment'] = [
    'label'             => &$GLOBALS['TL_LANG']['tl_iso_payment']['billbee_payment']
,   'exclude'           => true
,   'inputType'         => 'select'
,   'options'           => [1, 2, 3, 4, 6, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48]
,   'reference'         => &$GLOBALS['TL_LANG']['tl_iso_payment']['billbee_payments']
,   'eval'              => ['includeBlankOption'=>true, 'chosen'=>true, 'tl_class'=>'w50']
,   'sql'               => "varchar(8) NOT NULL default ''"
];
