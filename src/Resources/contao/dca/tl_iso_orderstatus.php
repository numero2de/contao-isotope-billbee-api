<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2019 Leo Feyer
 *
 * @package   Isotope Billbee Api
 * @author    Benny Born <benny.born@numero2.de>
 * @author    Michael Bösherz <michael.boesherz@numero2.de>
 * @license   LGPL
 * @copyright 2019 numero2 - Agentur für digitales Marketing
 */


/**
 * Add palettes to tl_iso_orderstatus
 */
$GLOBALS['TL_DCA']['tl_iso_orderstatus']['palettes']['default'] .= ';{billbee_legend:hide},billbee_status';


/**
 * Add fields to tl_iso_orderstatus
 */
$GLOBALS['TL_DCA']['tl_iso_orderstatus']['fields']['billbee_status'] = [
    'label'             => &$GLOBALS['TL_LANG']['tl_iso_orderstatus']['billbee_status']
,   'exclude'           => true
,   'inputType'         => 'select'
,   'options'           => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
,   'reference'         => &$GLOBALS['TL_LANG']['tl_iso_orderstatus']['billbee_states']
,   'eval'              => ['includeBlankOption'=>true, 'tl_class'=>'w50']
,   'sql'               => "varchar(8) NOT NULL default ''"
];
