<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2019 Leo Feyer
 *
 * @package   Isotope Billbee Api
 * @author    Benny Born <benny.born@numero2.de>
 * @author    Michael Bösherz <michael.boesherz@numero2.de>
 * @license   LGPL
 * @copyright 2019 numero2 - Agentur für digitales Marketing
 */


/**
 * Namespace
 */
namespace numero2\IsotopeBillbeeApi\DCAHelper;

use Contao\DataContainer;
use Contao\System;
use Contao\Backend;


class IsotopePayment extends Backend {


    /**
     * Adds additon billbee fields to all palettes
     *
     * @param string $varValue
     * @param DataContainer $dc
     *
     * @return string
     */
    public function onload( DataContainer $dc) {

        foreach( $GLOBALS['TL_DCA']['tl_iso_payment']['palettes'] as $key => $palette ) {

            if( in_array($key, ['__selector__', 'default']) ) {
                continue;
            }

            if( strpos($GLOBALS['TL_DCA']['tl_iso_payment']['palettes'][$key], 'billbee_legend') === false ) {
                $GLOBALS['TL_DCA']['tl_iso_payment']['palettes'][$key] = str_replace(
                    ';{enabled_legend'
                ,   ';{billbee_legend:hide},billbee_payment;{enabled_legend'
                ,   $GLOBALS['TL_DCA']['tl_iso_payment']['palettes'][$key]
                );
            }
        }
    }

}
