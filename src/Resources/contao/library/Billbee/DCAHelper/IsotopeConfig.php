<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2019 Leo Feyer
 *
 * @package   Isotope Billbee Api
 * @author    Benny Born <benny.born@numero2.de>
 * @author    Michael Bösherz <michael.boesherz@numero2.de>
 * @license   LGPL
 * @copyright 2019 numero2 - Agentur für digitales Marketing
 */


/**
 * Namespace
 */
namespace numero2\IsotopeBillbeeApi\DCAHelper;

use Contao\DataContainer;
use Contao\System;
use Contao\Backend;


class IsotopeConfig extends Backend {


    /**
     * Calculates the api key based on secret and config id
     *
     * @param string $varValue
     * @param DataContainer $dc
     *
     * @return string
     */
    public function generateApiKey($varValue, DataContainer $dc) {

        if( empty($varValue) ) {
            $secret = System::getContainer()->getParameter('kernel.secret');

            return md5(hash_hmac("sha256", utf8_encode($secret), utf8_encode($dc->id)));
        }

        return $varValue;
    }
}
